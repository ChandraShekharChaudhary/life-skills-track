# Prevention of Sexual Harassment

### Q1.  What kinds of behaviour cause sexual harassment?

sexual misconduct consists of:

* Displaying a sexual gaze or leer towards another person.
* pursuing or proposing unwelcome sexual relations.
* Making jokes or sexual remarks.
* Making inappropriate or unwelcome physical contact with another person.
* Making explicit sexual content visible in the workplace or other public areas.
* Making sexually provocative or humiliating gestures or speech.
* Displaying inappropriate sexual gestures or facial expressions.
* Threatening or intimidating someone based on their gender or sexual orientation.

### Q2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?

The following actions should be taken by me if I am being harassed:

* I will keep a record of the episodes, including dates, times, places, what was said or done, and any witnesses, and I will document the harassment.

* I'll speak up and make an effort to be assertive I should tell the harasser directly that their actions are undesirable and inappropriate.

* I'll file a report of the harassment I immediately notify a manager or administration if the harassment occurs at work or school.

* Seeking emotional support or speaking with a dependable friend, member of your family, or workmate.



### Q3. Explains different scenarios enacted by actors.

Various scenarios were carried out by actors:

**Workplace Harassment:**  

Kavita works in an office where her coworker, rohit, frequently sends explicit texts, makes improper sexual remarks about her attractiveness, and inappropriately touches her. This is regarded as sexual harassment and makes Kavita's workplace uncomfortable.

**Street harassment:** 

Usha is being catcalled by a group of males as she crosses the street, who are making graphic and vulgar remarks about her body. Usha feels uncomfortable and threatened by their unwelcome, disrespectful behaviour. This is an illustration of sexual harassment that takes the form of street harassment.

**Cyberbullying:**

Emma is a proponent of an online gaming community. She is, however, subjected to ongoing abuse from another gamer who sends her lewd messages, criticises her gender, and threatens her with sexual assault. Online harassment of this nature is regarded as sexual harassment.

**Harassment in an Academic Setting:**

Maya notes that David, a classmate, frequently enters her personal space, touches her improperly, and makes unwelcome approaches towards her during a lecture. Maya's capacity to concentrate and participate in her studies is impacted by this behaviour in addition to making the learning atmosphere uncomfortable.

**Harassment at a Social Event:** 

Lisa goes to a party with her pals. A man named Mike constantly tries to dance sensually with her during the occasion.

### Q4. How to handle cases of harassment?

Although handling harassment claims can be a challenging and intricate procedure, there are steps that can be taken:

* **React quickly:** React quickly if you witness or experience harassment. This may entail telling the individual to stop.

* Keep a record of the occurrence, including the time, date, place, and details of what transpired.

* **Report the incident:** If the harassment happened at work, let your employer or the human resources division know about it. If it occurs outside of work, get in touch with the police.

* **Seek support:** Harassment can be upsetting and traumatic. Consult a dependable family member, friend, or counsellor for support.

* Take action to stop such occurrences from happening again if you're a manager or employer.

### Q5. How to behave appropriately?

* Show respect and dignity to everyone, regardless of gender or sexual orientation.
* Refrain from making remarks that can be interpreted as sexual in nature about someone's looks, body, or attire.
* Never approach someone sexually or make unwanted physical contact with them without their permission.
* Avoid using sexually explicit language or telling anecdotes or jokes about sexual content.
* Honour the boundaries and personal space of others.
* If someone complains that your actions have made them uncomfortable or harassed, pay attention to their concerns and provide an apology.