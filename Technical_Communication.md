# SOLID: The First 5 Principles of Object-Oriented Design

## Abstract:


This technical paper explores the SOLID principles of object-oriented design: 
* Single Responsibility Principle (SRP), 
* Open-Closed Principle (OCP), 
* Liskov Substitution Principle (LSP), 
* Interface Segregation Principle (ISP), and 
* Dependency Inversion Principle (DIP). 

In the context of a challenging project codebase, this paper investigates the importance of SOLID principles in achieving maintainable, flexible, and extensible software. JavaScript code samples are provided to illustrate the practical implementation of each principle, and additional resources are referenced for further study.

### Introduction:

The acronym originated by Robert C. Martin, and it stands for five principles of Object-Oriented Programming. These are a set of rules for certain design patterns and software architecture.

In the realm of software development, managing complex codebases can be an arduous task. When faced with an intricate project codebase, developers encounter challenges in terms of maintainability, extensibility, and overall code quality. The SOLID principles offer a set of guidelines to address these challenges and promote well-designed object-oriented software.

### 1. Single Responsibility Principle (SRP):
The Single Responsibility Principle emphasizes that a class should have a single responsibility or reason to change. This principle advocates for separation of concerns and encourages modular code. We provide JavaScript code samples to illustrate a violation of the SRP and subsequently refactor the code to adhere to the principle.

```
Bad design violating SRP:

class User {
  save() {
    // code for saving user to the database
  }

  sendEmail(message) {
    // code for sending email
  }

  generateReport() {
    // code for generating a user report
  }
}

Good design following SRP:

class User {
  save() {
    // code for saving user to the database
  }
}

class EmailSender {
  sendEmail(message) {
    // code for sending email
  }
}

class ReportGenerator {
  generateReport() {
    // code for generating a user report
  }
}
```

  ### 2. Open-Closed Principle (OCP):
The Open-Closed Principle suggests that software entities should be open for extension but closed for modification. It promotes designing modules that can be easily extended without modifying their existing code. JavaScript code examples are presented to demonstrate a violation of the OCP and the subsequent improvement achieved through refactoring.

```
class Shape {
  constructor(type) {
    this.type = type;
  }

  area() {
    if (this.type === 'circle') {
      // Calculation for circle area
    } else if (this.type === 'rectangle') {
      // Calculation for rectangle area
    } else if (this.type === 'triangle') {
      // Calculation for triangle area
    }
  }
}
```
In the above example, the Shape class violates the OCP because it is not closed for modification. If a new shape is introduced, such as a square, we would need to modify the Shape class by adding another else if statement.

```
class Shape {
  constructor() {}

  area() {
    throw new Error('Method not implemented');
  }
}

class Circle extends Shape {
  constructor(radius) {
    super();
    this.radius = radius;
  }

  area() {
    return Math.PI * this.radius ** 2;
  }
}

class Rectangle extends Shape {
  constructor(width, height) {
    super();
    this.width = width;
    this.height = height;
  }

  area() {
    return this.width * this.height;
  }
}

class Triangle extends Shape {
  constructor(base, height) {
    super();
    this.base = base;
    this.height = height;
  }

  area() {
    return (this.base * this.height) / 2;
  }
}
```
In this refactored version, the Shape class is an abstract base class with an area method that throws an error by default. It serves as a blueprint for other shape classes to inherit from.

The Circle, Rectangle, and Triangle classes extend the Shape class and provide their own implementation of the area method specific to each shape.


### 3. Liskov Substitution Principle (LSP):
The Liskov Substitution Principle (LSP) is one of the SOLID principles in object-oriented programming. It states that objects of a superclass should be replaceable with objects of its subclasses without affecting the correctness of the program. In other words, derived classes should be able to substitute their base classes seamlessly.

```
class Square extends Rectangle {
  constructor(sideLength) {
    super(sideLength, sideLength);
  }

  setWidth(width) {
    this.width = width;
    this.height = width;
  }

  setHeight(height) {
    this.width = height;
    this.height = height;
  }
}

```
In this code, the Square class extends the Rectangle class, which may seem logical at first since a square is a special case of a rectangle. However, the Square class introduces additional methods setWidth and setHeight that violate the behavior expected from a rectangle.

### 4. Interface Segregation Principle (ISP):
The Interface Segregation Principle emphasizes that clients should not be forced to depend on interfaces they do not use. It encourages the creation of cohesive and minimalistic interfaces tailored to specific client requirements. We present JavaScript code examples to illustrate ISP violations and propose interface segregation through refactoring techniques.
```
class MediaPlayer {
  play() {
    throw new Error('Method not implemented');
  }

  pause() {
    throw new Error('Method not implemented');
  }

  stop() {
    throw new Error('Method not implemented');
  }

  rewind() {
    throw new Error('Method not implemented');
  }

  fastForward() {
    throw new Error('Method not implemented');
  }
}
```
```
class AudioPlayer extends MediaPlayer {
  play() {
    console.log('Playing audio');
  }

  pause() {
    console.log('Pausing audio');
  }

  stop() {
    console.log('Stopping audio');
  }

  rewind() {
    // Not applicable to audio, do nothing
  }

  fastForward() {
    // Not applicable to audio, do nothing
  }
}
```
```
class VideoPlayer extends MediaPlayer {
  play() {
    console.log('Playing video');
  }

  pause() {
    console.log('Pausing video');
  }

  stop() {
    console.log('Stopping video');
  }

  rewind() {
    console.log('Rewinding video');
  }

  fastForward() {
    console.log('Fast-forwarding video');
  }
}
```

In this example, we have a MediaPlayer class that serves as the base interface for different types of media players. It defines methods like play, pause, stop, rewind, and fastForward.

The AudioPlayer class extends MediaPlayer and implements the necessary methods for playing audio. However, the rewind and fastForward methods are not applicable to audio, so they are implemented as empty methods.

The VideoPlayer class also extends MediaPlayer and implements all the methods. In this case, both rewind and fastForward have meaningful implementations for video.

### 5. Dependency Inversion Principle (DIP):
The Dependency Inversion Principle advocates for high-level modules to depend on abstractions, rather than concrete implementations. This principle promotes loose coupling, flexibility, and ease of maintainability. JavaScript code samples demonstrate the application of DIP through dependency injection and inversion of control.

```
// Abstraction
class IUserRepository {
  getUser(userId) {
    throw new Error('Method not implemented');
  }

  saveUser(user) {
    throw new Error('Method not implemented');
  }
}

// High-level module
class UserService {
  constructor(userRepository) {
    this.userRepository = userRepository;
  }

  getUser(userId) {
    return this.userRepository.getUser(userId);
  }

  saveUser(user) {
    this.userRepository.saveUser(user);
  }
}

// Low-level module
class UserRepository extends IUserRepository {
  getUser(userId) {
    // Database query to get user by ID
  }

  saveUser(user) {
    // Database operation to save user
  }
}
```
In this refactored code, the UserService class receives an instance of IUserRepository through its constructor, enabling it to work with any implementation of the interface.

The UserRepository class now extends IUserRepository and implements the required methods.


### Conclusion:
The SOLID principles provide essential guidelines for designing robust and maintainable object-oriented software. By adhering to these principles, developers can enhance code readability, modularity, extensibility, and testability. Through JavaScript code samples and practical illustrations, we have demonstrated the application of each SOLID principle. Further exploration of these principles will empower developers to refactor and improve existing codebases while ensuring long-term software sustainability.

Note: For code samples and additional resources, please refer to the provided links: References.

### References:

1. SOLID: The First 5 Principles of Object Oriented Design, By Samuel Oloruntoba, published on September 21, 2020. [DigitalOcean](https://www.digitalocean.com/community/conceptual-articles/)

2. Solid Design Principle, video lecture on youtube by kudvenkat, on Jan 17, 2020 [website](https://www.youtube.com/playlist?list=PL6n9fhu94yhXjG1w2blMXUzyDrZ_eyOme) 

3. S.O.L.I.D The first 5 principles of Object Oriented Design with JavaScript by Cristian Ramirez on medium.com published on Jan 10, 2017 `https://medium.com/@cramirez92/s-o-l-i-d-the-first-5-priciples-of-object-oriented-design-with-javascript-790f6ac9b9fa`

4. FreeCodeCamp : SOLID Principles for Programming and Software Design, by Joel Olawanle,Published on NOVEMBER 9, 2022, `https://www.freecodecamp.org/news/solid-principles-for-programming-and-software-design/`



