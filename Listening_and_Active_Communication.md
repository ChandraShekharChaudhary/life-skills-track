## Active Listening

### 1. What are the steps/strategies to do Active Listening?

Ans:- There are following steps/strategies to do Active Listening:-

* Face the speaker and maintain eye contact.
* Don’t start planning what to say next.
* Be attentive, but relaxed.
* Listen to the words and try to picture what the speaker is saying.
* Wait for the speaker to pause to ask clarifying questions.
* Try to feel what the speaker is feeling. Show that you’re listening


## Reflective Listening

### 2. According to Fisher's model, what are the key points of Reflective Listening?

Ans:- According to Fisher's model, there are four key points of Reflective Listening:-

Orientation:- Orientation is the first step in which we try to know each other.
Conflict:- Conflict is the second step in which each individual try to come up with a solution of their own perspective.
Emergence:- Emergence is the third step in which  emergence, most of the responses are kept quiet with most of them and a leader will be raised for the team who gives opinions of the strike, whether it is favorable or unfavorable to others.
Reinforcement:- Reinforcement is the final step in which every members try to observe their final decision through an array of their own viewpoints.


## Reflection

### 3. What are the obstacles in your listening process?

Ans:- there are few obstacles in my listening process:-


* Distractions: External noises, background music, or even your own thoughts can be a distraction that prevents you from fully concentrating on what someone is saying.


* Prejudice or bias: Sometimes, we may have preconceived notions about the speaker, their opinions, or their background, which can prevent us from truly listening with an open mind.


* Technical issues: Sometimes, technical issues such as connectivity problems or server downtime can interfere with my ability to receive and process input.

```Today I met a friend. He said that my room rent is less than yours. My roommate and I went to a friend's house. I noticed that her room is very small and not well maintained, but at that time my roommate and I said wow! I'm liking it. But when we returned to our room we discussed among ourselves that his room was very bad.```

* Language barriers: Communication can break down when there is a language barrier between the speaker and listener, leading to misunderstandings or misinterpretations.

Differences in communication styles: People have different communication styles, and when these styles clash, it can be challenging to understand each other.



### 4. What can you do to improve your listening?


* Regular updates with new data and information
* Maintain eye contact with the speaker
* Visualize what the speaker is saying.
* Pay attention to nonverbal signs, such as body language and tone
* Empathise with the speaker


## Types of Communication

### 5. When do you switch to Passive communication style in your day to day life?

Ans:- Passive communication is mainly used for keeping the peace, especially when dealing with conflict.

For example:- A man asks in a restaurant for a steak made very, and when the waiter brings it, it is little made. When the waiter asks if everything is to his liking, the man responds affirmatively.



### 6. When do you switch into Aggressive communication styles in your day to day life?

Ans:- When I know that who is saying me is correct but second person is not ready to understand my point. On that time I switch into Aggressive communication styles.

### 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?

Ans:- Today I met a friend. He said that my room rent is less than yours. My roommate and I went to a friend's house. I noticed that her room is very small and not well maintained, but at that time my roommate and I said wow! I'm liking it. But when we returned to our room we discussed among ourselves that his room was very bad. On that time I switch into Passive Aggressive ommunication styles.

### 8. How can you make your communication assertive?

Ans:- When I was in the online meeting, at that time my friend told me that let's go for a walk. I said my meeting ends in 10 minutes, please wait for 10 minutes, after that we will go out together. On that time I make my communication assertive.