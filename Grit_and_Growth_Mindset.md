# Grit and Growth Mindset

## 1.Grit

### Question 1. 
#### Paraphrase (summarize) the video in a few lines. Use your own words.

**Ans.** 
* The video is about how IQ alone does not determine a student's performance. Instead passion and perseverance for long-term goals, which is defined as grit, plays a crucial role. 
* Considering factors like family income and test scores, students with more grit are more likely to graduate and excel. 
* Research is needed to test and measure the effectiveness of different approaches in cultivating grit in children.

### Question 2. 
#### What are your key takeaways from the video to take action on?

**Ans.** My key takeaways from the video to take action on are:

* To learn from failures and be persistent in pursuing effective methods to cultivate passion and perseverance for long-term goals.
* Need of a motivational and psychological perspective is needed in education.
* The ability to learn is not fixed and can improve with effort.

## 2. Introduction to Growth Mindset


### Question 3. 
#### Paraphrase (summarize) the video in a few lines in your own words.

**Ans.** 
* Individuals with a fixed mindset typically avoid or resist engaging with the ingredients of effort, challenges, mistakes, and feedback, whereas individuals with a growth mindset welcome and value them as chances to grow and improve. 
* Mindsets exist on a spectrum and can vary depending on the situation, rather than being fixed. 
* By comprehending the qualities and actions related to each mindset, individuals can determine their position on the spectrum and analyze the underlying beliefs and focus that contribute to their mindset.

### Question 4.
#### What are your key takeaways from the video to take action on?

**Ans.** Key takeaway points from the videos are :

* **Growth mindset:** Carol Dweck's concept of growth mindset suggests that people's beliefs about skill and development play a crucial role in their ability to learn and succeed.

* **Characteristics of mindsets:** The growth mindset believes that skills are built, focuses on the process of learning and improvement, embraces effort, challenges, mistakes, and feedback as opportunities for growth, and believes in the capacity to learn and develop.

* **Importance of growth mindset:** The growth mindset serves as the foundation for effective learning.

* **Four key ingredients to growth:** Effort, challenges, mistakes, and feedback are identified as the key ingredients for growth and learning.

* **Creating a culture for learning:** To foster a culture of learning, it is essential to zoom in on beliefs and focus.

## 3. Understanding Internal Locus of Control


### Question 5.
#### What is the Internal Locus of Control? What is the key point in the video?

**Ans** The internal locus of control refers to the belief that individuals have control over their lives and are responsible for the outcomes they experience. It emphasizes personal effort and actions as the driving factors behind success or failure.

- The key point in the video are:

*  Having an internal locus of control is crucial for maintaining motivation.
*  The study mentioned in the video demonstrated that individuals who believed their success was attributed to their hard work and effort displayed higher levels of motivation and perseverance.
*  On the other hand, those who believed their success was due to external factors, such as being inherently smart or gifted, exhibited lower motivation and a lack of willingness to take on challenging tasks.
*  The video suggests that adopting an internal locus of control by taking responsibility for one's actions and achievements can lead to increased motivation and a sense of control over one's life.

## 3. How to build a Growth Mindset


### Question 6.
#### Paraphrase (summarize) the video in a few lines in your own words.

**Ans.**
* A growth mindset begins with the fundamental rule of life, which is believing in our ability to  figure things out. If we believe that we can figure things out and get better, it enables us to pursue lifelong improvement and growth.
* The second thing to do to develop a greater growth mindset is to question our assumptions. 
* Don't let your current knowledge, skills, and abilities box in or narrow down your vision for the future because today has little to do with what you're capable of achieving in the future.




### Question 7.
#### What are your key takeaways from the video to take action on?

**Ans.** Key takeaways are:

* **Believe in your ability to figure things out:** Embrace the belief that you have the capacity to learn, improve, and overcome challenges.
* **Question your assumptions:** Challenge any fixed beliefs or assumptions that may be holding you back.
* **Create your own learning curriculum:** Take responsibility for your personal growth by designing your own learning path.
* **Embrace the struggle:** Instead of shying away from challenges or setbacks, view them as opportunities for growth.
* **Cultivate resilience:** Develop resilience by maintaining a positive attitude and persevering in the face of difficulties.
* **Seek feedback and learn from criticism:** Embrace feedback as a valuable tool for growth.

## 4. Mindset - A MountBlue Warrior Reference Manual

### Question 8.
#### What are one or more points that you want to take action on from the manual? (Maximum 3)

**Ans.** Points that I want to take action on from the manual are:

* I am 100 percent responsible for my learning.
* I will be focused on mastery till I can do things half asleep. If someone wakes me up at 3 AM in the night, I will calmly solve the problem like James Bond and go to sleep.
* I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.